const entity = {
    name: 'Payment',
    collection: 'payments',

    fields: [
        {
            name: 'user_id', type: String
        },
        {
            name: 'amount', type: String
        },
        {
            name: 'distance_user_id', type: String
        },
        {
            name: 'dt', type: Date
        },
        {
            name: 'status', type: String
        }
    ]
}

module.exports = entity;