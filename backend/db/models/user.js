const entity = {
    name: 'User',
    collection: 'users',

    fields: [
        {
            name: 'username', type: String
        },
        {
            name: 'email', type: String
        },
        {
            name: 'password', type: String
        },
        {
            name: 'balance', type: Number
        }
    ]
}

module.exports = entity;