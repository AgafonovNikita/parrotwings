const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');

const db_url = 'mongodb://localhost/parrotWings';

const db = {
    models: {},
    update: async (entityName, payload) => {

        const model = db.models[entityName];

        // TODO set spec error
        if (!model) throw new Error({ code: 'Invalid entity name: ' + entityName });

        let result;

        if (payload._id) {
            result = await model.updateOne({ _id: payload._id }, payload);
        }
        else {
            result = await model.create(payload);
        }

        return result;
    },

    getList: async (entityName, payload) => {

        const model = db.models[entityName];

        // TODO set spec error
        if (!model) throw new Error({ code: 'Invalid entity name: ' + entityName });


        return await model.find({ ...payload });

    }
};

const init = () => {

    mongoose.connect(db_url, { useNewUrlParser: true, useUnifiedTopology: true });

    db.connection = mongoose.connection;
    db.connection.on('error', console.error.bind(console, 'connection error:'));
    db.connection.once('open', function () {
        console.log('db connected');
    });

    const fileNames = fs.readdirSync(path.join(__dirname, 'models'));

    for (const fileName of fileNames) {
        const fileNameParts = fileName.split('.');
        if (fileNameParts.length === 2 && fileNameParts[1] === 'js') {

            const { name: entityName, fields, collection } = require('./models/' + fileName);

            const schema = {};

            for (const field of fields) {
                schema[field.name] = field.type;
            }

            db.models[entityName] = mongoose.model(entityName, new mongoose.Schema(schema), collection);
        }
    }
}


init();




module.exports = db;