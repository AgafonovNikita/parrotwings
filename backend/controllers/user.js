
const Auth = require('./auth');

const db = require('../db');
const C = require('../constants');


class User {

    static async getList(req) {
        const { user } = await Auth.check(req);

        const users = await db.getList('User');

        return users;
    }

    static async get(req) {
        const { user } = await Auth.check(req);
        return user;
    }

    static async createPayment(req, options) {
        const { user } = await Auth.check(req);
        const { amount, userId } = options;

        if (user.balance < amount) throw new Error('Less than necessary balance');

        const now = new Date();

        const payment = await db.update('Payment', {
            user_id: user._id,
            amount,
            distance_user_id: userId,
            status: 'success',
            dt: now
        });

        const sourceUser = await db.update('User', {
            _id: user._id,
            balance: user.balance - amount,
        });

        const distUser = await db.update('User', {
            _id: userId,
            balance: user.balance + amount,
        });

        const updatedUser = (await db.getList('User', { _id: user._id }))[0];

        return {
            user: updatedUser,
            payment,
        }
    }

    static async getPayments(req, options) {
        const { user } = await Auth.check(req);

        const payments = await db.getList('Payment', { user_id: user._id });

        const userIds = payments.map(x => x.distance_user_id);

        const users = await db.getList('User', { _id: userIds });

        const result = [];

        for (const payment of payments) {
            const resultItem = payment.toObject();
            const distanceUser = users.find(x => x._id.toString() === payment.distance_user_id);
            resultItem.DistanceUser = distanceUser ? distanceUser.toObject() : {};
            result.push(resultItem);
        }

        return result;
    }
}

module.exports = User;