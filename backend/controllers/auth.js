const db = require('../db');
const C = require('../constants');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const JWT_SECRET = C.JWSONWEBTOKEN_KEY;

const login = async (options) => {
    const { email, password } = options;

    const users = await db.getList('User', { email: email.toLowerCase() });

    if (users.length === 0) throw new Error(C.ERROR_CODE_USER_NOT_FOUND);

    const user = users[0].toObject();

    try {
        const comparePassword = await bcrypt.compare(password, user.password);
        if (!comparePassword) {
            throw new Error(C.ERROR_CODE_INVALID_PASSWORD);
        }
    } catch (e) {
        throw new Error(C.ERROR_CODE_INVALID_PASSWORD)
    }

    delete user.password;

    const token = jwt.sign(user, JWT_SECRET);

    return { token, user };
}


const logout = async () => {

}

const check = async (req) => {
    const authToken = req.headers.authorization;
    const token = authToken.replace('Bearer ', '');

    const decodedUser = jwt.verify(token, JWT_SECRET);

    const users = await db.getList('User', { _id: decodedUser._id });

    if (users.length === 0) {
        throw new Error(C.ERROR_CODE_USER_NOT_FOUND);
    }

    return { user: users[0] };
}

const registration = async (options) => {
    const { username, email, password } = options;

    const users = await db.getList('User', { email: email.toLowerCase() });


    if (users.length !== 0) throw new Error(C.ERROR_CODE_EMAIL_ALREADY_EXISTS);

    const salt = bcrypt.genSaltSync();
    const _password = bcrypt.hashSync(password, salt);

    const user = (
        await db.update('User', {
            username,
            email,
            password: _password,
            balance: C.BALANCE_START
        })
    ).toObject();

    delete user.password;

    const token = jwt.sign(user, JWT_SECRET);

    return { token, user };
}


module.exports = { login, logout, registration, check };