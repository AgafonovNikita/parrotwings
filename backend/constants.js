module.exports = {
    ERROR_CODE_EMAIL_ALREADY_EXISTS: 'Email already exists',
    ERROR_CODE_USER_NOT_FOUND: 'User not found',
    ERROR_CODE_INVALID_PASSWORD: 'Invalid password',

    BALANCE_START: 500,
    
    // TODO TRANSFER TO ENV or DB
    JWSONWEBTOKEN_KEY: 'PARROT WINGS'
}