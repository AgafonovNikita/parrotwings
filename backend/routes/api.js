const express = require('express');
const router = express.Router();

const Auth = require('../controllers/auth');
const User = require('../controllers/user');


router.post('/login', (req, res, next) => {
    Auth.login(req.body)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});

router.get('/check', (req, res, next) => {
    Auth.check(req)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});

router.post('/logout', (req, res, next) => {
    Auth.logout(req)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});

router.post('/registration', (req, res, next) => {
    Auth.registration(req.body)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});


router.put('/payments/:id/commit', (req, res, next) => {

});

router.put('/payments/:id/refund', (req, res, next) => {

});

router.get('/payments', (req, res, next) => {
    User.getPayments(req)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});


router.get('/users', (req, res, next) => {

    User.getList(req)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});

router.get('/user', (req, res, next) => {

    User.get(req)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});

router.post('/payments', (req, res, next) => {

    User.createPayment(req, req.body)
        .then((data) => {
            res.send(data);
        })
        .catch(
            (err) => {
                res.status(500).send({ error: err.message });
            }
        )
});

module.exports = router;
