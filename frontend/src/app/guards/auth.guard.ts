import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { AuthService } from '../auth.service';
import { switchMap } from 'rxjs/operators';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private auth: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {

        return new Promise((resolve, reject) => {
            this.auth.check()
                .subscribe(
                    data => {
                        if (data === true) {
                            return resolve(true);
                        }
                        this.router.navigate(['/login'], {
                            queryParams: {
                                return: state.url
                            }
                        });
                        resolve(false);
                    },
                    err => reject(false));
        });
    }
}
