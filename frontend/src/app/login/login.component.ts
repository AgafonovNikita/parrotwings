import { Component, OnInit, OnDestroy } from '@angular/core';
import { ValidateService, CheckValid } from '../validate.service';
import { ApiService } from '../api.service';
import { takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public password: string;
  public email: string;

  public validEmail: CheckValid;
  public validPassword: CheckValid;

  public authorizationError = false;

  private alive = true;

  constructor(
    private validate: ValidateService,
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  login() {

    this.authorizationError = false;

    this.validEmail = this.validate.checkEmail(this.email);
    this.validPassword = this.validate.checkPassword(this.password);

    if (this.validEmail.valid && this.validPassword.valid) {

      this.auth.login(this.email, this.password).pipe(takeWhile(() => this.alive))
        .subscribe(
          () => this.router.navigate(['/']),
          err => {
            this.authorizationError = true;

          }
        );
    }
  }

}
