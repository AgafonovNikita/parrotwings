export interface Payment {
    _id?: string;
    user_id: string;
    distance_user_id: string;
    amount: number;
    dt: Date;
}
