import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { User } from './models/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiUrl = environment.api_url;

  constructor(private http: HttpClient) { }

  getReqOption() {
    const headers = new HttpHeaders();
    headers.append('Authorization', `Bearer ${localStorage.getItem('id')}`);
    headers.append('Content-Type', 'application/json');

    const options = {
      // withCredentials: true,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('id')}`
      },
    };

    return options;
  }

  getUsers() {
    return this.http.get(this.apiUrl + '/users', this.getReqOption());
  }

  getUser() {
    return this.http.get(this.apiUrl + '/user', this.getReqOption());
  }

  createPayment(userId: string, amount: number) {
    return this.http.post(this.apiUrl + '/payments', { userId, amount }, this.getReqOption());
  }

  getPayments() {
    return this.http.get(this.apiUrl + '/payments', this.getReqOption());
  }
}
