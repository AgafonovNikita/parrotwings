import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  constructor() { }

  checkEmail(email: string): CheckValid {

    let message;

    if (!email) {
      message = 'Enter email';
    } else {
      if (!email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/)) {
        message = 'Invalid email';
      }
    }

    return message ? { valid: false, message } : { valid: true };
  }

  checkPassword(password: string, isRegistration: boolean = false): CheckValid {
    let message;

    if (!password) {
      message = 'Enter password';
    } else {
      if (isRegistration && password.length < 7) {
        message = 'Your password isn’t strong enough, try making it longer.';
      }
    }

    return message ? { valid: false, message } : { valid: true };
  }

  checkReplyPassword(password: string, replyPassword: string): CheckValid {
    let message;

    if (password !== replyPassword) {
      message = 'Password mismatch';
    }

    return message ? { valid: false, message } : { valid: true };
  }

  checkUsername(username: string): CheckValid {
    let message;

    if (!username) {
      message = 'Enter username';
    }

    return message ? { valid: false, message } : { valid: true };
  }

  checkAmount(amount: string): CheckValid {
    let message;

    if (!amount) {
      message = 'Enter amount';
    } else if (!amount.match(/^[0-9]*$/)) {
      message = 'Amount is not number';
    }

    return message ? { valid: false, message } : { valid: true };
  }


  checkUserId(userId: string): CheckValid {
    let message;

    if (!userId) {
      message = 'Select user';
    }

    return message ? { valid: false, message } : { valid: true };

  }
}


export interface CheckValid {
  valid: boolean;
  message?: string;
}
