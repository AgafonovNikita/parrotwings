import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { Observable, from, Observer } from 'rxjs';
import { Router } from '@angular/router';
import { User } from './models/user';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authUrl = environment.api_url;

  user: User;

  constructor(private http: HttpClient, private router: Router) {
  }

  getReqOption() {
    const headers = new HttpHeaders();
    headers.append('Authorization', `Bearer ${localStorage.getItem('id')}`);
    headers.append('Content-Type', 'application/json');

    const options = {
      // withCredentials: true,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('id')}`
      },
    };

    return options;
  }

  check(): Observable<any> {
    return from(new Promise((resolve, reject) => {
      if (this.user) { return resolve(true); }

      const token = localStorage.getItem('id');

      if (!token) {
        return resolve(false);
      }

      this.http.get(this.authUrl + '/check', this.getReqOption()).toPromise()
        .then(
          (data: any) => {
            this.saveLoginData(data);
            resolve(true);
          },
          err => {
            console.error(err);
            resolve(false);
          }
        );
    }));
  }

  login(email: string, password: string): Observable<any> {
    return new Observable((observer: Observer<any>) => {
      this.http.post(this.authUrl + '/login', { email, password }
      )
        .subscribe((data: any) => {
          localStorage.setItem('id', data.token);
          this.saveLoginData(data);

          observer.next(data);
          observer.complete();
        }, (err: any) => {
          observer.error(err);
        });
    });
  }

  logout(navigate: boolean = false): Observable<any> {
    this.user = null;

    return new Observable((observer: Observer<any>) => {
      this.http.post(this.authUrl + '/logout', {})
        .subscribe(
          (data: any) => {
            observer.next(data);
            localStorage.removeItem('id');
            if (navigate) {
              this.router.navigateByUrl('/login');
            }
            observer.complete();
          },
          (err) => { observer.error(err); },
          () => {

          });
    });
  }

  reg(username: string, email: string, password: string): Observable<any> {
    return new Observable((observer: Observer<any>) => {
      this.http.post(this.authUrl + '/registration', { username, password, email })
        .subscribe(
          (data: any) => {
            localStorage.setItem('id', data.token);
            this.saveLoginData(data);

            observer.next(data);
            observer.complete();
          },
          (err: any) => {
            observer.error(err);
          },
          () => { }
        );
    });
  }


  saveLoginData({ user }) {
    console.log('user', user);
    this.user = user;
  }

}
