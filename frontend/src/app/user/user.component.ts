import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../models/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public user: User;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.user = this.auth.user;
  }

  createPayment() {

  }

  logout() {
    this.auth.logout(true).subscribe();
  }

}
