import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../models/user';
import { ApiService } from '../api.service';
import { takeWhile, switchMap } from 'rxjs/operators';
import { CheckValid, ValidateService } from '../validate.service';
import { of } from 'rxjs';

@Component({
  selector: 'app-create-payment',
  templateUrl: './create-payment.component.html',
  styleUrls: ['./create-payment.component.scss']
})
export class CreatePaymentComponent implements OnInit, OnDestroy {

  public user: User;
  public users: User[];

  public selectedUserId: string;
  public amount: string;

  public validUserId: CheckValid;
  public validAmount: CheckValid;

  private alive = true;

  public loaded = false;

  public paymentDone = false;
  public distUser: User;

  public paymentError = '';

  constructor(private auth: AuthService, private api: ApiService, private validate: ValidateService) { }

  ngOnInit() {
    this.user = this.auth.user;

    this.init();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  init() {
    this.api.getUsers()
      .pipe(takeWhile(() => this.alive))
      .subscribe(
        (data: User[]) => {
          this.users = data.filter(x => x._id !== this.user._id);
          this.loaded = true;
        }
      );
  }

  create() {

    this.validAmount = this.validate.checkAmount(this.amount);
    this.validUserId = this.validate.checkUserId(this.selectedUserId);

    this.paymentError = '';

    if (this.validAmount.valid && this.validUserId.valid) {

      this.api.getUser()
        .pipe(takeWhile(() => this.alive),
          switchMap((user: User) => {
            this.user.balance = user.balance;

            console.log(user);


            if (user.balance < +this.amount) {
              this.validAmount = { valid: false, message: 'Less than necessary balance' };
              return of(null);
            }

            return this.api.createPayment(this.selectedUserId, +this.amount);
          })
        )
        .subscribe(
          (data) => {
            if (!data) { return; }

            const { user, payment } = data;
            this.user.balance = user.balance;
            this.auth.user.balance = user.balance;

            this.paymentDone = true;
            this.distUser = this.users.find(x => x._id === this.selectedUserId);
          },
          err => {
            this.paymentError = typeof err.error === 'string' ? err.error : err.error.error;
          }
        );

    }
  }
}
