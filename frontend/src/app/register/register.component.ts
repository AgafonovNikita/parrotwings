import { Component, OnInit, OnDestroy } from '@angular/core';
import { CheckValid, ValidateService } from '../validate.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  public password: string;
  public email: string;
  public username: string;
  public replyPassword: string;

  public validEmail: CheckValid;
  public validPassword: CheckValid;
  public validReplyPassword: CheckValid;
  public validUsername: CheckValid;

  public signupError = '';

  private alive = true;

  constructor(
    private validate: ValidateService,
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  signup() {

    this.signupError = '';

    this.validEmail = this.validate.checkEmail(this.email);
    this.validPassword = this.validate.checkPassword(this.password, true);
    this.validUsername = this.validate.checkUsername(this.username);
    this.validReplyPassword = this.validate.checkReplyPassword(this.password, this.replyPassword);

    if (this.validEmail.valid && this.validPassword.valid && this.validUsername.valid && this.validReplyPassword.valid) {

      this.auth.reg(this.username, this.email, this.password).pipe(takeWhile(() => this.alive))
        .subscribe(
          () => this.router.navigate(['/']),
          err => {
            this.signupError = typeof err.error === 'string' ? err.error : err.error.error;
          }
        );
    }
  }
}
