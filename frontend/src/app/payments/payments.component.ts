import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { ApiService } from '../api.service';
import { ValidateService } from '../validate.service';
import { User } from '../models/user';
import { takeWhile } from 'rxjs/operators';
import { Payment } from '../models/payment';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit, OnDestroy {

  public user: User;
  public payments: Payment[];

  public loaded = false;
  private alive = true;

  constructor(private auth: AuthService, private api: ApiService, private validate: ValidateService) { }

  ngOnInit() {
    this.user = this.auth.user;

    this.init();
  }

  ngOnDestroy() {
    this.alive = false;
  }


  init() {
    this.api.getPayments()
      .pipe(takeWhile(() => this.alive))
      .subscribe(
        (data: Payment[]) => {
          console.log(data);
          this.payments = data;
          this.loaded = true;
        }
      );
  }

}
